#include "spec.h"

#include <iostream>

int main()
{
	std::vector<std::string> vec;
	MomoSpec ms;

	ms.loadMacroFile("./tests/macro.yaml");

	vec = ms.getAllMacroNodes();

	for (size_t i = 0; i < vec.size(); i++) {
		ms.defineMacro(vec[i], ms.getMacroYAMLValue(vec[i]));
		std::cout << ms.getMacroDefinition(vec[i]) << std::endl;
	}

	std::cout << ms.getMacroYAMLValue("make_build") << std::endl; // should fail

	return 0;
}
