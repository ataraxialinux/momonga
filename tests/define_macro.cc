#include "spec.h"

#include <iostream>

int main()
{
	MomoSpec ms;

	ms.defineMacro("installroot", "/ew");

	std::cout << ms.getMacroDefinition("installroot") << std::endl;
	std::cout << ms.getMacroDefinition("installroota") << std::endl; // should abort

	return 0;
}
