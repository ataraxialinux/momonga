#include "spec.h"

#include <iostream>

int main()
{
	MomoSpec ms;

	ms.loadSpecFile("./tests/recipe_split.yaml");
	ms.loadMacroFile("./tests/macro.yaml");

	ms.defineMacro("build", "x86_64-linux-musl");
	ms.defineMacro("configure", "./configure --build=%build");
	ms.defineMacro("make_build", "make");
	ms.defineMacro("make_install", "make install");
	ms.defineMacro("buildroot", "./BUILDROOT");
	ms.defineMacro("prefix", "/usr");
	ms.defineMacro("bindir", "%prefix/bin");

	ms.loadMacroDefines();
	ms.loadSpecDefines();

	ms.tokenizeRecipe();

	std::cout << ms.getTokenizedSpecPath() << std::endl;

	return 0;
}
