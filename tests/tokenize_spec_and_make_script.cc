#include "spec.h"

#include <iostream>

int main()
{
	MomoSpec ms;

	ms.loadSpecFile("./tests/recipe.yaml");
	ms.loadMacroFile("./tests/macro.yaml");

	ms.defineMacro("buildroot", "./BUILDROOT");
	ms.defineMacro("build", "x86_64-linux-musl");
	ms.defineMacro("configure", "./configure --build=%build");
	ms.defineMacro("make_build", "make");
	ms.defineMacro("make_install", "make DESTDIR=\"%buildroot\" install");
	ms.defineMacro("prefix", "/usr");
	ms.defineMacro("bindir", "%prefix/bin");
	ms.loadMacroDefines();
	ms.loadSpecDefines();

	ms.tokenizeRecipe();
	ms.loadSpecTokenizedFile(ms.getTokenizedSpecPath());
	ms.generateScript();

	std::cout << ms.getTokenizedSpecPath() << std::endl;
	std::cout << ms.getScriptPath() << std::endl;

	return 0;
}
