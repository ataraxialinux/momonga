#include "spec.h"

#include <iostream>

int main()
{
	std::vector<std::string> vec;
	MomoSpec ms;

	ms.loadMacroFile("./tests/macro.yaml");

	vec = ms.getAllMacroNodes();

	for (size_t i = 0; i < vec.size(); i++) {
		std::cout << i << " " << vec[i] << std::endl;
	}

	return 0;
}
