#include "utils.h"

#include <filesystem>

#include <stdlib.h>

namespace Momo {

std::string getFdPath(int fd)
{
	if (fd == -1)
		throw std::runtime_error("Cannot access tokenize recipe");

	return std::filesystem::read_symlink(std::filesystem::path("/proc/self/fd") / std::to_string(fd));
}

} // Momo
