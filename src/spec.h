#ifndef _SPEC_H
#define _SPEC_H

#include <yaml-cpp/yaml.h>

#include <string>
#include <map>
#include <vector>
#include <fstream>

#include <unistd.h>

class MomoSpec {
public:
	~MomoSpec();
	void loadMacroFile(const std::string& file);
	void loadMacroDefines();
	void loadSpecDefines();
	void loadSpecFile(const std::string& file);
	void loadSpecTokenizedFile(const std::string& file);
	void defineMacro(const std::string& key, const std::string& value);
	void tokenizeRecipe();
	void generateScript();
	std::string replaceMacro(const std::string& value);
	std::string getMacroDefinition(const std::string& key);
	std::string getMacroYAMLValue(const std::string& key);
	std::string getSpecYAMLValue(const std::string& key);
	std::string getTokenizedSpecYAMLValue(const std::string& key);
	std::string getTokenizedSpecPath();
	std::string getScriptPath();
	std::vector<std::string> getAllMacroNodes();
	std::vector<std::string> getAllSpecNodes();
	std::vector<std::string> getAllTokenizedSpecNodes();

private:
	int spec_fd;
	int script_fd;
	void findTokenizedValueAndWrite(FILE *fp, std::vector<std::string>& v, const std::string& key);
	void writeBase(FILE *fp);
	void writeEnd(FILE *fp);
	std::map<std::string, std::string> macros;
	std::ifstream spec;
	std::ifstream spec_tokenized;
	std::string getYAMLValue(const std::string& key, YAML::Node node);
	std::vector<std::string> getAllNodes(YAML::Node node);
	YAML::Node macro_yaml;
	YAML::Node spec_yaml;
	YAML::Node spec_tokenized_yaml;
};

#endif
