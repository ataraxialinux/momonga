#ifndef _DOWNLOAD_H
#define _DOWNLOAD_H

#include <string>
#include <vector>

class MomoLoad {
public:
	~MomoLoad();
	void setupSourceDir(const std::string& dir);
	void addFilesToDownload(const std::string& files);

private:
	std::string sourceDir;
	std::vector<std::string> toDownlaod;
};

#endif
