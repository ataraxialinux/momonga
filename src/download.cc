#include "download.h"

#include <stdexcept>
#include <sstream>
#include <regex>

#include <sys/stat.h>

MomoLoad::~MomoLoad()
{
}

void MomoLoad::setupSourceDir(const std::string& dir)
{
	struct stat st;

	if (dir.empty())
		throw std::runtime_error("Download directory path is empty");

	if (stat(dir.c_str(), &st) != 0)
		throw std::runtime_error("Download directory does not exist");

	sourceDir = dir;
}

void MomoLoad::addFilesToDownload(const std::string& files)
{
	std::string token;
	std::stringstream ss(files);
	std::regex expression("^((https|http|ftp|rtsp|mms|file)?:\\/\\/).+");

	while (ss >> token) {
		if (std::regex_match(token.begin(), token.end(), expression))
			toDownlaod.push_back(token);
	}
}
