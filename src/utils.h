#ifndef _UTILS_H
#define _UTILS_H

#include <string>

namespace Momo {

std::string getFdPath(int fd);

} // Momo

#endif
