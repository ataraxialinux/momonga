#include "spec.h"
#include "utils.h"

#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <filesystem>

#include <sys/stat.h>

MomoSpec::~MomoSpec()
{
	std::string internal_spec = getTokenizedSpecPath();
	std::string internal_script = getScriptPath();

	if (spec.is_open())
		spec.close();

	if (spec_tokenized.is_open())
		spec_tokenized.close();

	if (spec_fd != -1)
		close(spec_fd);

	if (script_fd != -1)
		close(script_fd);

	remove(internal_spec.c_str());
	remove(internal_script.c_str());
}

std::string MomoSpec::getMacroDefinition(const std::string& key)
{
	auto it = macros.find(key);

	try {
		if (it == macros.end())
			throw std::runtime_error("cannot find macro by this key");
	} catch (std::exception& e) {
		throw;
	}

	return it->second;
}

std::string MomoSpec::replaceMacro(const std::string& value)
{
	std::string ret = value;
	std::string token;
	std::stringstream ss(ret);
	const char restricted[] = "\'\"\\/()<>[]{}+-=|,.:;?!@$^&*";

	while (ss >> token) {
		if (token.find("%") != std::string::npos) {
			for (auto x : restricted) {
				std::replace(token.begin(), token.end(), x, ' ');
			}

			std::string token2;
			std::stringstream ss2(token);
			while (ss2 >> token2) {
				if (token2.find("%") != std::string::npos) {
					std::string nopercent = token2;

					nopercent.erase(std::remove(nopercent.begin(), nopercent.end(), '%'), nopercent.end());
					ret.replace(ret.find(token2), token2.length(), getMacroDefinition(nopercent));
				}
			}
		}
	}

	return ret;
}

void MomoSpec::defineMacro(const std::string& key, const std::string& value)
{
	std::string value2 = value;

	if (value2.find("%") != std::string::npos) {
		value2 = replaceMacro(value2);
	}

	macros.insert(std::pair<std::string, std::string>(key, value2));
}

void MomoSpec::loadMacroFile(const std::string& file)
{
	macro_yaml = YAML::LoadFile(file);
}

void MomoSpec::loadSpecFile(const std::string& file)
{
	spec.open(file, std::ifstream::in);
	spec_yaml = YAML::LoadFile(file);
}

void MomoSpec::loadSpecTokenizedFile(const std::string& file)
{
	spec_tokenized.open(file, std::ifstream::in);
	spec_tokenized_yaml = YAML::LoadFile(file);
}

void MomoSpec::loadMacroDefines()
{
	std::vector<std::string> vec = getAllMacroNodes();

	for (std::string& x : vec) {
		defineMacro(x, getMacroYAMLValue(x));
	}
}

void MomoSpec::loadSpecDefines()
{
	std::vector<std::string> vec = getAllSpecNodes();

	for (std::string& x : vec) {
		defineMacro(x, getSpecYAMLValue(x));
	}
}

std::vector<std::string> MomoSpec::getAllNodes(YAML::Node node)
{
	std::vector<std::string> res;

	for (YAML::const_iterator i = node.begin(); i != node.end(); i++) {
		res.push_back(i->first.as<std::string>());
	}

	return res;
}

std::string MomoSpec::getYAMLValue(const std::string& key, YAML::Node node)
{
	try {
		if (!node[key])
			throw std::runtime_error("cannot find node in file by this key");
	} catch (std::exception& e) {
		throw;
	}

	return node[key].as<std::string>();
}

std::vector<std::string> MomoSpec::getAllMacroNodes()
{
	return getAllNodes(macro_yaml);
}

std::string MomoSpec::getMacroYAMLValue(const std::string& key)
{
	return getYAMLValue(key, macro_yaml);
}

std::vector<std::string> MomoSpec::getAllSpecNodes()
{
	return getAllNodes(spec_yaml);
}

std::string MomoSpec::getSpecYAMLValue(const std::string& key)
{
	return getYAMLValue(key, spec_yaml);
}

std::vector<std::string> MomoSpec::getAllTokenizedSpecNodes()
{
	return getAllNodes(spec_tokenized_yaml);
}

std::string MomoSpec::getTokenizedSpecYAMLValue(const std::string& key)
{
	return getYAMLValue(key, spec_tokenized_yaml);
}

std::string MomoSpec::getTokenizedSpecPath()
{
	return Momo::getFdPath(spec_fd);
}

std::string MomoSpec::getScriptPath()
{
	return Momo::getFdPath(script_fd);
}

void MomoSpec::tokenizeRecipe()
{
	char filetmp[] = "/tmp/momo-yaml.XXXXXXX";
	FILE *fp;
	std::string line;
	std::string to_open;

	spec_fd = mkstemp(filetmp);
	if (spec_fd == -1)
		throw std::runtime_error("Failed to create temporary file");

	to_open = getTokenizedSpecPath();

	fp = fopen(to_open.c_str(), "w");
	if (!fp)
		throw std::runtime_error("Failed to open temporary file");

	while (getline(spec, line)) {
		std::string newline;

		if (line.find("%") != std::string::npos) {
			newline = replaceMacro(line) + "\n";
		} else {
			newline = line + "\n";
		}

		fputs(newline.c_str(), fp);
	}

	fclose(fp);
}

void MomoSpec::writeBase(FILE *fp)
{
	fputs("#!/bin/sh\n", fp);
	fputs("set -ex\n\n", fp);
}

void MomoSpec::writeEnd(FILE *fp)
{
	fputs("\n", fp);
	fputs("exit 0\n\n", fp);
}

void MomoSpec::findTokenizedValueAndWrite(FILE *fp, std::vector<std::string>& v, const std::string& key)
{
	std::string line;

	if (std::find(v.begin(), v.end(), key) != v.end()) {
		std::string token = getTokenizedSpecYAMLValue(key);
		fputs(token.c_str(), fp);
		fputs("\n", fp);
	}
}

void MomoSpec::generateScript()
{
	char filetmp[] = "/tmp/momo-script.XXXXXXX";
	FILE *fp;
	std::string to_open;
	std::vector<std::string> v;

	script_fd = mkstemp(filetmp);
	if (script_fd == -1)
		throw std::runtime_error("Failed to create temporary file");

	to_open = getScriptPath();

	fp = fopen(to_open.c_str(), "w");
	if (!fp)
		throw std::runtime_error("Failed to open temporary file");

	v = getAllTokenizedSpecNodes();

	writeBase(fp);
	findTokenizedValueAndWrite(fp, v, "unpack");
	findTokenizedValueAndWrite(fp, v, "prepare");
	findTokenizedValueAndWrite(fp, v, "configure");
	findTokenizedValueAndWrite(fp, v, "compile");
	findTokenizedValueAndWrite(fp, v, "test");
	findTokenizedValueAndWrite(fp, v, "install");
	writeEnd(fp);

	int i = fchmod(script_fd, S_IRWXU);
	if (i == -1)
		throw std::runtime_error("Failed to change mode of generated script");

	fclose(fp);
}
